const MAX_ATTEMPTS = 10;
let currentURL = document.location.href;

// Logging In Toast
function showToast() {
	Toastify({
	  text: "Attempting login...",
	  backgroundColor: "linear-gradient(to right, #cc5500, #ff791a)",
	  className: "info",
	  position: "right",
	}).showToast();	
}


// Canvas Landing Page
if (currentURL.startsWith('https://canvas.utexas.edu')) {
	showToast();
	console.log("Canvas Auto-Login | Found Canvas Landing Page");
	let currentAttempt = 1;

	let keepAttempting = setInterval(function () {
		try {
			let enterButton = document.getElementsByClassName("canvas-button")[0];
			enterButton.click();
			clearInterval(keepAttempting);
		} catch(err) {
			if(currentAttempt == 1)
				console.log(err);

			console.log('Canvas Auto-Login | Searching for Canvas Login Button | Attempt #' + currentAttempt);
		}

		if (currentAttempt == MAX_ATTEMPTS) {
			console.log('Canvas Auto-Login | Search Failed :(');
			clearInterval(keepAttempting);
		}
		currentAttempt += 1;

	}, 200);
}

// Canvas Login Page
else if (currentURL.startsWith('https://enterprise.login.utexas.edu') && !currentURL.includes('Logout')) {
	showToast();
	console.log("Canvas Auto-Login | Found Canvas Login Page");
	let currentAttempt = 1;

	const keepAttempting = setInterval(function () {
		try {
			let username = document.getElementById('username');
			let password = document.getElementById('password');

			if (username && password) {
				username.click();
				password.click();
			}

			if (username.value && password.value || (currentAttempt == MAX_ATTEMPTS && username && password)) {
				let submitButton = document.getElementById("login-button").getElementsByTagName('input')[0];
				submitButton.click();
				clearInterval(keepAttempting);
			} else {
				console.log('Canvas Auto-Login | Form Is Not Auto-Filled');
			}
		} catch (err) {
			if(currentAttempt == 1) {
				console.log(err);
			}

			console.log('Canvas Auto-Login | Searching for Form Fields and Login Button | Attempt #' + currentAttempt);
		}

		if (currentAttempt == MAX_ATTEMPTS) {
			console.log('Canvas Auto-Login | Search Failed :(');
			clearInterval(keepAttempting);
		}
		currentAttempt += 1;

	}, 200);
}

// General UT Login Page
else if (currentURL.startsWith('https://login.utexas.edu')) {
	showToast();
	console.log("Canvas Auto-Login | Found General UT Login Page");
	let currentAttempt = 1;

	let keepAttempting = setInterval(function () {
		try {
			let username = document.getElementById("IDToken1");
			let password = document.getElementById("IDToken2");

			if (username && password) {
				username.click();
				password.click();
			}

			if (username.value && password.value || (currentAttempt == MAX_ATTEMPTS && username && password)) {
				let submitButton = document.getElementById("login_btn").getElementsByTagName('input')[0];
				submitButton.click();
				clearInterval(keepAttempting);
			} else {
				console.log('Canvas Auto-Login | Form Is Not Auto-Filled');
			}
		} catch(err) {
			if(currentAttempt == 1) {
				console.log(err);
			}

			console.log('Canvas Auto-Login | Searching for Form Fields and Login Button | Attempt #' + currentAttempt);
		}

		if (currentAttempt == MAX_ATTEMPTS) {
			console.log('Canvas Auto-Login | Search Failed :(');
			clearInterval(keepAttempting);
		}
		currentAttempt += 1;

	}, 200);
}
